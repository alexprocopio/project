package com.ucsd.placeit.main;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.ucsd.placeit.model.PlaceIt;
import com.ucsd.placeit.model.PlaceItBank;
import com.ucsd.placeit.model.PlaceItBank.PlaceItIterator;
import com.ucsd.placeit.model.PlaceItBankListener;
import com.ucsd.placeit.util.Consts;

//import android.location.LocationListener;

public class LocationService extends Service implements
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener, LocationListener,
		PlaceItBankListener {

	// flag for GPS status
	boolean isGPSEnabled = false;

	// flag for network status
	boolean isNetworkEnabled = false;

	boolean canGetLocation = false;

	Location location; // location
	double latitude; // latitude
	double longitude; // longitude

	// Declaring a Location Manager
	protected LocationManager mLocationManager;
	private LocationClient mLocationClient;
	private LocationRequest mLocationRequest;
	public static final String BROADCAST_ACTION = "com.ucsd.placeit.main";
	Intent intent;
	int counter = 0;
	private boolean mActivityEnabled;

	// ------- Private variables to be used in the logic ------\\
	/**
	 * Place it bank to store all the current placeIts
	 */
	private PlaceItBank mPlaceItBank;
	private List<PlaceIt> mProximityList;

	@Override
	public void onCreate() {
		super.onCreate();

		// Initlizes new placeIt bank
		mPlaceItBank = new PlaceItBank(getApplicationContext());

		Log.d(Consts.TAG, "Location service create");

		int resp = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resp == ConnectionResult.SUCCESS) {
			mLocationClient = new LocationClient(this, this, this);
			mLocationClient.connect();

		} else {
			Toast.makeText(this, "Google Play Service Error " + resp,
					Toast.LENGTH_LONG).show();

		}
		intent = new Intent(BROADCAST_ACTION);
		mProximityList = new ArrayList<PlaceIt>();
	}

	@Override
	public void onStart(Intent intent, int startId) {
		Log.d(Consts.TAG, "Location service start");

		// Setting the activity active state
		if (intent.hasExtra(Consts.EXTRA_ACTIVITY_ONLINE)) {
			mActivityEnabled = intent.getExtras().getBoolean(
					Consts.EXTRA_ACTIVITY_ONLINE);
			Log.d(Consts.TAG_NOTIFY, "Activity is on? " + mActivityEnabled);
		}

		// Checking if the local db has to be updated
		if (intent.hasExtra(Consts.EXTRA_UPDATE_ID)) {
			Log.d(Consts.TAG_NOTIFY, "Updating of Service");

			
			int placeItId = intent.getExtras().getInt(
					Consts.EXTRA_UPDATE_ID);
			int updateState = intent.getExtras().getInt(
					Consts.EXTRA_UPDATE_STATE);
			int options = intent.getExtras().getInt(
					Consts.EXTRA_UPDATE_OPTIONS, 0);
			mPlaceItBank.updatePlaceItBank(placeItId, updateState, options);
		}

		if (mLocationClient != null && mLocationClient.isConnected()) {
			mLocationRequest = LocationRequest.create();
			mLocationRequest
					.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
			// Set the update interval to 5 seconds
			mLocationRequest.setInterval(Consts.UPDATE_INTERVAL);
			// Set the fastest update interval to 1 second
			mLocationRequest.setFastestInterval(Consts.FASTEST_INTERVAL);
			mLocationClient.requestLocationUpdates(mLocationRequest, this);
		}

	}

	@Override
	public void onConnected(Bundle connectionHint) {
		Log.d(Consts.TAG, "Location service connected");
		if (mLocationClient != null && mLocationClient.isConnected()) {
			mLocationRequest = LocationRequest.create();
			mLocationRequest
					.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

			// Set the update interval to 5 seconds
			mLocationRequest.setInterval(Consts.UPDATE_INTERVAL);
			// Set the fastest update interval to 1 second
			mLocationRequest.setFastestInterval(Consts.FASTEST_INTERVAL);
			mLocationClient.requestLocationUpdates(mLocationRequest, this);
		}
	}

	@Override
	public void onDestroy() {
		Log.d(Consts.TAG, "Location service destroyed");

		mLocationClient.removeLocationUpdates(this);
		mLocationClient.disconnect();
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	/**
	 * Function to show settings alert dialog
	 * */
	public void showSettingsAlert() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				getApplicationContext());

		// Setting Dialog Title
		alertDialog.setTitle("GPS is settings");

		// Setting Dialog Message
		alertDialog
				.setMessage("GPS is not enabled. Do you want to go to settings menu?");

		// On pressing Settings button
		alertDialog.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						getApplicationContext().startActivity(intent);
					}
				});

		// on pressing cancel button
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

	/**
	 * Define the callback method that receives location updates
	 */
	@Override
	public void onLocationChanged(Location location) {
		Log.d(Consts.TAG, "Location changed from service!");

		ArrayList<PlaceIt> newList = checkInProximity(location, Consts.ACTIVE);

		if (newList.size() > 0) {
			// Convert to a list of IDs
			int size = newList.size();
			int[] idArray = new int[size];
			for (int i = 0; i < size; i++) {
				idArray[i] = newList.get(i).getId();
				if (!mActivityEnabled) {
					createNotification(newList.get(i));
				}
				Log.d(Consts.OTHER_TAG, "USER MOVED INTO RADIUS OF "
						+ newList.get(i).getTitle());
			}

			// Passing the intent
			Intent intent = new Intent(BROADCAST_ACTION);
			intent.putExtra(Consts.EXTRA_IN_PROX_ID, idArray);
			sendBroadcast(intent);

			// Intent intent2 = new Intent(BROADCAST_ACTION);
			// intent2.putExtra(Consts.EXTRA_LATITUDE, location.getLatitude());
			// intent2.putExtra(Consts.EXTRA_LONGITUDE,
			// location.getLongitude());
			// intent2.putExtra(Consts.EXTRA_PROVIDER, location.getProvider());
			// sendBroadcast(intent2);
		}
	}

	/**
	 * Creates notification based on proximity. Will make sure the activity is
	 * not running, so only will display when it is in the background.
	 * 
	 * @param placeIt
	 */
	@SuppressWarnings("deprecation")
	private void createNotification(PlaceIt placeIt) {
		Log.d(Consts.TAG_NOTIFY, "create new notification");
		NotificationManager notificationManager;
		String title = placeIt.getTitle();
		String subject = placeIt.getDesc();
		String body = String.format(Consts.MESSAGE_NOTIFICATION,
				placeIt.getTitle());

		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notify = new Notification(
				android.R.drawable.stat_notify_more, title,
				System.currentTimeMillis());
		notify.flags |= Notification.FLAG_AUTO_CANCEL;

		// PendingIntent pending = PendingIntent.getActivity(
		// getApplicationContext(), 0, new Intent(), 0);
		/* Creates an explicit intent for an Activity in your app */

		Intent resultIntent = new Intent(this, MainActivity.class);
		resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);

		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		stackBuilder.addParentStack(MainActivity.class);
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
				PendingIntent.FLAG_UPDATE_CURRENT);
		notify.setLatestEventInfo(getApplicationContext(), subject, body,
				resultPendingIntent);
		notificationManager.notify(placeIt.getId(), notify);
	}

	/**
	 * Check the proximity of proximity of the location passed in
	 * 
	 * @param location
	 * @param state
	 * @return
	 */
	private ArrayList<PlaceIt> checkInProximity(Location location, int state) {
		PlaceItIterator iterator = mPlaceItBank.iterator(Consts.ACTIVE);
		PlaceIt placeIt;
		float[] results = new float[1];
		ArrayList<PlaceIt> newList = new ArrayList<PlaceIt>();
		int count = 0;
		// Check to see whether there are more placeIts to compare
		while (iterator.hasNext()) {
			placeIt = iterator.next();
			count++;
			Location.distanceBetween(location.getLatitude(),
					location.getLongitude(), placeIt.getCoord().latitude,
					placeIt.getCoord().longitude, results);

			// Log.d(Consts.OTHER_TAG,
			// "Distance from placeIT: " + Float.toString(results[0]));

			// Log.d(Consts.OTHER_TAG, "proximitysize" + mProximityList.size());

			boolean contains = false;
			// Cycle through each of the placeIts in that are already near
			if (results[0] < Consts.PLACEIT_RADIUS) {
				// Check through the proximity list to see if it is inside
				// already
				for (int i = 0; i < mProximityList.size(); i++) {
					// Expand proximity list
					if (placeIt.equals(mProximityList.get(i))) {
						Log.d(Consts.OTHER_TAG, "Already contained " + count
								+ "id = " + placeIt.getId());
						contains = true;
						break;
					}
				}
				if (!contains) {
					Log.d(Consts.OTHER_TAG,
							"Added " + count + "id = " + placeIt.getId()
									+ " , placeIt Name: " + placeIt.getTitle());
					mProximityList.add(placeIt);
					newList.add(placeIt);
				}
			} else {
				// Shrink proximity list
				for (int i = 0; i < mProximityList.size(); i++) {
					if (placeIt.equals(mProximityList.get(i))) {
						Log.d(Consts.OTHER_TAG, "Removed " + count + "id = "
								+ placeIt.getId());
						mProximityList.remove(i);
					}
				}
			}
		}
		return newList;
	}

	@Override
	public void onPlaceItBankChange(int placeItId, int updateState, int options) {
		mPlaceItBank.updatePlaceItBank(placeItId, updateState, options);
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
	}

}