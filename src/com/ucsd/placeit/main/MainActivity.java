package com.ucsd.placeit.main;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.CancelableCallback;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ucsd.placeit.R;
import com.ucsd.placeit.controller.CentralLogicController;
import com.ucsd.placeit.model.PlaceIt;
import com.ucsd.placeit.model.PlaceItBank;
import com.ucsd.placeit.model.PlaceItBankListener;
import com.ucsd.placeit.uimodules.NewPlaceItActivity;
import com.ucsd.placeit.util.Consts;

public class MainActivity extends Activity implements OnMapClickListener,
		CancelableCallback, GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener, LocationListener,
		PlaceItBankListener, OnInfoWindowClickListener {

	private GoogleMap mMap;
	private LocationClient mLocationClient;
	private LocationRequest mLocationRequest;
	private Location location;
	private List<Marker> mMarkers;
	private Iterator<Marker> marker;
	private Intent intent;
	private LatLng newMarkerPosition;
	private Marker markerOnFocus;

	// Alarm Manager variables
	PendingIntent pi;
	BroadcastReceiver br;
	AlarmManager am;

	public PlaceItBank mPlaceItBank;
	private CentralLogicController mCic;

	/**
	 * onCreate method to start the main activity and the map
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		// Set up the map
		this.setUpMapIfNeeded();

		// Create a new instance of centralLogicController
		mCic = CentralLogicController.getInstance(this);
		mCic.registerListener(this);
		Log.d(Consts.TAG, "STARTED");

		// Create and initialize a new placeItBank every time it has started
		mPlaceItBank = new PlaceItBank(this);

		mMap.setMyLocationEnabled(true);
		mMap.setOnMapClickListener(this);
		mMap.setOnInfoWindowClickListener(this);

		// Set the markers from the PlaceItBank
		Log.d(Consts.TAG, "Setting markers on map");
		mMarkers = setAllMarkers();

		mLocationClient = new LocationClient(this, this, this);
		mLocationRequest = LocationRequest.create();
		// Use high accuracy
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		// Set the update interval to 5 seconds
		mLocationRequest.setInterval(Consts.UPDATE_INTERVAL);
		// Set the fastest update interval to 1 second
		mLocationRequest.setFastestInterval(Consts.FASTEST_INTERVAL);

		// Setup Alarm Manager
		setupAlarmManager();

		// Creating the location service
		Log.d(Consts.TAG, "call to start service");
		// startService(new Intent(getBaseContext(), LocationService.class));

	}

	/**
	 * Sets up the alarm manager for recurring placeIts
	 */
	private void setupAlarmManager(){
        br = new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent data) {
                // TODO Auto-generated method stub
                // Retrieve necessary data from intent  
                int id = data.getIntExtra(Consts.RESULT_SET_ID, 0);
                
                PlaceIt placeit = mCic.getPlaceIt(id);
                Log.d("Debug",placeit.getPostDate().toString());
                String title = placeit.getTitle();
                String sid = String.valueOf(id);
                LatLng location = placeit.getCoord();
                addNewMarkerToMap(location,title,sid,BitmapDescriptorFactory.HUE_RED);
            }
        };  
        
        registerReceiver(br, new IntentFilter(Consts.INTENT_DELAYED_NAME));
        am = (AlarmManager)(this.getSystemService(Context.ALARM_SERVICE));      
    }

	/**
	 * returns a list of new markers and also displays the markers on the map.
	 * 
	 * @return
	 */
	private List<Marker> setAllMarkers() {
		List<Marker> markerList = new ArrayList<Marker>();
		PlaceIt placeIt;
		LatLng coord;
		String title, id;
		Iterator<PlaceIt> iterator = mPlaceItBank.iterator(Consts.ACTIVE);
		while (iterator.hasNext()) {
			Log.d(Consts.TAG, "Iterating through placeitbank");
			placeIt = iterator.next();
			Log.d(Consts.TAG, "  geting coord");
			coord = placeIt.getCoord();
			Log.d(Consts.TAG, "  getting title");
			title = placeIt.getTitle();
			Log.d(Consts.TAG, "  getting id");
			id = Integer.toString(placeIt.getId());

			Log.d(Consts.TAG, "  display on map");
			if (placeIt.getState() == Consts.ACTIVE) {
				MarkerOptions newMarkerOptions = new MarkerOptions()
						.position(coord).title(title).snippet(id);
				Marker addedPlaceIt = mMap.addMarker(newMarkerOptions);
				markerList.add(addedPlaceIt);
			}
		}
		return markerList;
	}

	@Override
	public void onPause() {
		super.onPause();
		// Tells that the activity is not in the foreground
		intent = new Intent(MainActivity.this, LocationService.class);
		intent.putExtra(Consts.EXTRA_ACTIVITY_ONLINE, false);
		startService(intent);
	}

	@Override
	public void onStop() {
		super.onStop();
		

		// //Unregister the content receiver
	}

	@Override
	public void onResume() {
		super.onResume();
		// Tells that the main activity is in the foreground
		intent = new Intent(MainActivity.this, LocationService.class);
		intent.putExtra(Consts.EXTRA_ACTIVITY_ONLINE, true);
		startService(intent);
		// Call to register reciever
		registerReceiver(broadcastReceiver, new IntentFilter(
				LocationService.BROADCAST_ACTION));
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		stopService(new Intent(MainActivity.this, LocationService.class));
		// unregisterReceiver(broadcastReceiver);

		unregisterReceiver(br);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Connect to the location client
		mLocationClient.connect();


		// // Watch out for delayed reminder to become active
		registerReceiver(br, new IntentFilter(Consts.INTENT_DELAYED_NAME));
	}

	/**
	 * BroadcastReciever variable which registers a BroadcastReciever every time
	 * a new LocationService broadcast is sent.
	 */
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		/**
		 * Receiving the broadcasts from LocationServices
		 */
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d(Consts.TAG, "RECIVED TE BROADCAST REQUEST!");
			Bundle extrasBundle = intent.getExtras();

			// Retrieves the broadcast intArray from the reciever
			int[] idArray = extrasBundle.getIntArray(Consts.EXTRA_IN_PROX_ID);
			if (idArray.length > 0) {
				// Displays the popups
				display_popups(idArray, 0);
			}
		}
	};

	/**
	 * Recursive private method to split up the placeIt array
	 * 
	 * @param placeItIDs
	 *            the placeIt IDs to put in
	 * @param index
	 */
	private void display_popups(int[] placeItIDs, int index) {
		PlaceIt pi = mCic.getPlaceIt(placeItIDs[index]);
		String title = pi.getTitle();
		show_popup_window(placeItIDs[index], title);
		index++;
		if (index < placeItIDs.length) {
			display_popups(placeItIDs, index);
		}
	}

	/**
	 * Display the popup window to notify user of a reminder
	 * 
	 * @param placeItID
	 * @param title
	 */
	// Display the popup window to notify user of a reminder
    private void show_popup_window(final int placeItID, final String title){
        // Build the alert window
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Event: " + title);
        builder.setTitle("You have a reminder:");
        
        builder.setPositiveButton("Complete", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int dialogId){
                // Mark current reminder as complete
                mCic.changePlaceItState(placeItID, Consts.COMPLETE);
                // Remove it from map
                if(markerOnFocus != null)
                {
                    markerOnFocus.remove();
                    Toast.makeText(MainActivity.this, "Reminder is marked as complete..",
                            Toast.LENGTH_SHORT).show();
                }

                PlaceIt place = mCic.getPlaceIt(placeItID);
                // Check for recurring property
                if(place.getFrequency() > 0){
	                int newPiId = mCic.createRecurringReminder(placeItID);
	                if(newPiId >= 0){
	                    // A recurring reminder is created with solid ID
	                    PlaceIt placeit = mCic.getPlaceIt(newPiId);
	                    Intent outputData = new Intent();
	                    outputData.putExtra(Consts.RESULT_SET_NAME, placeit.getTitle());
	                    outputData.putExtra(Consts.RESULT_SET_ID, placeit.getId());
	                    outputData.putExtra(Consts.RESULT_SET_LAT, placeit.getCoord().latitude);
	                    outputData.putExtra(Consts.RESULT_SET_LNG, placeit.getCoord().longitude);
	                    // Task is in sleep mode
	                    // Get alarm manager and send in scheduled intent
	                    
	                    long miliTime = placeit.getPostDate().getTime();
	                    sendPendingIntent(outputData, miliTime);
	                }
                }
            }
            
        });
        builder.setNegativeButton("Delete", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id){
                // Delete marker
                Log.d(Consts.TAG, "Delete button hit");
                mCic.removePlaceIt(placeItID);  
                RemoveMarkers(placeItID);
                Toast.makeText(MainActivity.this, "Reminder is removed from map..", 
                        Toast.LENGTH_SHORT).show(); 
            }
        });
        builder.setNeutralButton("Remind Later", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id){
                // Do nothing, close alert dialog automatically.
            }
        });
        
        // Display the AlertDialog object and return it
        builder.show();
    }
    
    private void RemoveMarkers(int piID){
        for (int i = 0; i < mMarkers.size(); i++){
            if(mMarkers.get(i).getSnippet() == String.valueOf(piID)){
                mMarkers.get(i).remove();
            }
        }
    }

    // Send Future Reminder Intent at a scheduled Time
    void sendPendingIntent(Intent data, long miliTime){
        pi = PendingIntent.getBroadcast(this, Consts.DELAYED_INTENT_CODE, data, 0);
        long time = System.currentTimeMillis();
        Log.d("Debug", "System time: " + String.valueOf(time));
        Log.d("Debug", "Post time: " + String.valueOf(miliTime));
        am.set(AlarmManager.RTC, miliTime, pi);
    }

	@SuppressLint("NewApi")
	private void setUpMapIfNeeded() {
		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null) {
			mMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map)).getMap();
			// Check if we were successful in obtaining the map.
			if (mMap == null) {
				// Log this error
				Log.d(Consts.TAG, "Google Map Initiation Failed Miserably");
			}
		}
	}

	@Override
	public void onMapClick(LatLng position) {
		newMarkerPosition = position;
		Intent intent = new Intent(this, NewPlaceItActivity.class);
		Bundle args = new Bundle();
		args.putParcelable("Position", position);
		intent.putExtra("Position", args);
		startActivityForResult(intent, Consts.CREATE_NEW_MARKER);
	}

	/**
	 * Called when the infowindow for placeIts are clicked
	 */
	public void onInfoWindowClick(Marker marker) {
		markerOnFocus = marker;
		String sID = marker.getSnippet();
		int id = Integer.parseInt(sID);
		PlaceIt pi = mCic.getPlaceIt(id);
		String title = pi.getTitle();
		show_popup_window(id, title);
	}

	/**
	 * Returns the activity result from the new Activity form.
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(Consts.TAG_NOTIFY, "received create thing");
        
        if (requestCode == Consts.CREATE_NEW_MARKER) {
            if (resultCode == Consts.RESULT_SUCCESS) {
                // New Place it has been added to the database
                // Display only if reminder has active state
                String display = data.getStringExtra(Consts.RESULT_SET_DISPLAY);
                int placeitID = data.getIntExtra(Consts.RESULT_SET_ID, 0);
                PlaceIt pi = mCic.getPlaceIt(placeitID);
                if(display.equals("YES")){
                    // Display the reminders:
                    // Retrieve name from intent data
                    String title = pi.getTitle();
                    String sId = String.valueOf(placeitID);
                    addNewMarkerToMap(newMarkerPosition,title,sId,BitmapDescriptorFactory.HUE_RED);
                }
                else{
                    // Task is in sleep mode
                    // Get alarm manager and send in scheduled intent
                    long miliTime = pi.getPostDate().getTime();
                    sendPendingIntent(data, miliTime);
                }
            }
            // Otherwise the operation was canceled, show message
            else {
                Toast.makeText(MainActivity.this, "Operatrion canceled...",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

	/**
	 * Message gets the id of the new reminder being created.
	 * 
	 * @param position
	 *            position of the marker to be set
	 * @param title
	 *            the title of the message
	 * @param message
	 *            message content
	 * @param color
	 *            color
	 */
	// For now, message gets the id of the new reminder being created.
    private void addNewMarkerToMap(LatLng position, String title, String message, float color){
        Toast.makeText(MainActivity.this, "New Place-It added!",
                Toast.LENGTH_SHORT).show();
        MarkerOptions newMarkerOptions = new MarkerOptions()
                .position(position)
                .title(title)
                .snippet(message)
                .icon(BitmapDescriptorFactory.defaultMarker(color));
        Marker addedPlaceIt = mMap.addMarker(newMarkerOptions);
        mMarkers.add(addedPlaceIt);
    }

	@Override
	public void onCancel() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFinish() {
		if (marker.hasNext()) {
			Marker current = marker.next();
			mMap.animateCamera(
					CameraUpdateFactory.newLatLng(current.getPosition()), 2000,
					this);
			current.showInfoWindow();
		}
	}

	/**
	 * Location Callbacks - on location change does nothing
	 */
	@Override
	public void onLocationChanged(Location location) {
		// Report to the UI that the location was updated
		// mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
		// location.getLatitude(), location.getLongitude()), 18));
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		Log.d(Consts.TAG, "Connection failed");
	}

	/**
	 * Called when the google play services is connected
	 */
	@Override
	public void onConnected(Bundle arg0) {
		Log.d(Consts.TAG, "Connected to map activity");

		mLocationClient.requestLocationUpdates(mLocationRequest, this);
		location = mLocationClient.getLastLocation();

		if (location == null) {
			return;
		}

		// Retrieve the position and set it
		// Double latitude = location.getLatitude();
		// Double longitude = location.getLongitude();

		// CameraUpdate cameraPos = CameraUpdateFactory.newLatLng(new LatLng(
		// latitude, longitude));
		// mMap.moveCamera(cameraPos);

		// Animate the camera
		mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
				location.getLatitude(), location.getLongitude()), 18));
		// Toast.makeText(this, "Activity Connected!",
		// Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onDisconnected() {
		Log.d(Consts.TAG, "Disconnected Changed");

	}

	/**
	 * Listener method which checks to see if the bank has changed. Receives
	 * methods from the CentralLogicController.
	 */
	@Override
	public void onPlaceItBankChange(int placeItId, int updateState, int options) {
		// Update the place it bank
		mPlaceItBank.updatePlaceItBank(placeItId, updateState, options);
		
		Log.d(Consts.TAG_NOTIFY, "Updating bank");
		
		//Send the intent to service
		intent = new Intent(MainActivity.this, LocationService.class);
		intent.putExtra(Consts.EXTRA_UPDATE_ID, placeItId);
		intent.putExtra(Consts.EXTRA_UPDATE_STATE, updateState);
		intent.putExtra(Consts.EXTRA_UPDATE_OPTIONS, options);
		startService(intent);
		// switch (updateState) {
		// case Consts.UPDATE_ADD:
		//
		// case Consts.UPDATE_DELETE:
		// case Consts.UPDATE_STATE_UPDATE:
		// }
	}

}